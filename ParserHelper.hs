module ParserHelper where

import Parser hiding ((|||))

-- Functions for parsing.

(|||) :: Parser a -> Parser a -> Parser a
lfa ||| rfa = P $ \i ->
  case parse lfa i of
    Error _ -> parse rfa i
    r       -> r
infixl 3 ||| -- not previously defined

-- from Parser.hs in tutorials
chain :: Parser a -> Parser (a -> a -> a) -> Parser a
chain p op = p >>= rest
  where
    rest a = (||| pure a) $ do
      f <- op
      b <- p
      rest $ f a b

-- | like chain, but only allow a single parse, no more and no less - for unchainable infix operators
chain1 :: Parser a -> Parser (a -> a -> a) -> Parser a
chain1 p op = do
  a <- p
  f <- op
  b <- p
  pure $ f a b

-- from Parser.hs in tutorials
sepbyC :: Char -> Parser a -> Parser [a]
sepbyC c p = (:) <$> p <*> (list $ paddedC c >> p) ||| pure []

-- | Only ever retains the first parse, but tries to perform the second parse if possible.
trySecond :: Parser a -> Parser b -> Parser a
trySecond a b = a <* b ||| a

-- | Parse is successful after consuming 1+ ws, or reaching eof.
emptyOrSpace :: Parser ()
emptyOrSpace = P $ \i ->
  case parse (list1 space) i of
    Result j _          -> Result j ()
    Error UnexpectedEof -> Result i ()
    Error e             -> Error e

-- | Checks for a successful parse without consuming any input. Like |||, but backtracks on success.
check :: Parser a -> Parser ()
check p = P $ \i ->
  case parse p i of
    Result _ _ -> Result i ()
    Error e    -> Error e

alphanumeric :: String
alphanumeric = ['0'..'9'] ++ ['a'..'z'] ++ ['A'..'Z']

-- | Consumes the string as a whole, well-delimited word.
strWord :: String -> Parser String
strWord = between spaces (emptyOrSpace ||| check (noneof alphanumeric)) . string

-- | Helper function for `between`.
betweenChar :: Char -> Char -> Parser a -> Parser a
betweenChar l = between (is l) . is

-- | Pad something with optional spaces.
padded :: Parser a -> Parser a
padded = between spaces spaces

-- | Pad a character with optional spaces.
paddedC :: Char -> Parser Char
paddedC = padded . is

-- | Pad a string with optional spaces.
paddedS :: String -> Parser String
paddedS = padded . string

-- | Like anyof, but matching a string.
stringof :: String -> Parser String
stringof = list1 . oneof

-- | Explicit definition of `between` for parentheses padded with spaces.
betweenPaddedParens :: Parser a -> Parser a
betweenPaddedParens = between (paddedC '(') (paddedC ')')

-- | The above, but for brackets instead.
betweenPaddedBrackets :: Parser a -> Parser a
betweenPaddedBrackets = between (paddedC '[') (paddedC ']')

-- | Parse a positive integer using `reads`. When input has ints prefixed with 0s outside of a zero value, parse only a single 0.
int :: Parser Int
int = string "0" ||| list1 (oneof ['0'..'9'])
  >>= pure . (fst . head . reads)
