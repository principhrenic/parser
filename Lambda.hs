module Lambda where

import Data.Builder

-- Predefined lambdas.

-- | More readable infix operator for ap.
(%%) :: Builder -> Builder -> Builder
(%%) = ap
infixl 6 %%

-- Why let a good lambda parser go to waste?
-- toLambda :: String -> Builder
-- toLambda s = case parse lambdaP s of
--   Result _ l -> lamToBuilder l
--   _          -> error "bad lambda string provided"

-- ### Part 2, Excercise 1:

lambdaTrue :: Builder
lambdaTrue = boolToLam True -- lam 'x' $ lam 'y' $ term 'x'

lambdaFalse :: Builder
lambdaFalse = boolToLam False -- lam 'x' $ lam 'y' $ term 'y'

lambdaIf :: Builder
lambdaIf = lam 'b' $ lam 't' $ lam 'f' $
  term 'b' %% term 't' %% term 'f'

lambdaNot :: Builder
lambdaNot = lam 'x' $
  lambdaIf %% term 'x' %% lambdaFalse %% lambdaTrue

lambdaAnd :: Builder
lambdaAnd = lam 'x' $ lam 'y' $
  lambdaIf %% term 'x' %% term 'y' %% lambdaFalse

lambdaOr :: Builder
lambdaOr = lam 'x' $ lam 'y' $
  lambdaIf %% term 'x' %% lambdaTrue %% term 'y'

-- ### Part 2, Excercise 2:

lambdaSucc :: Builder
lambdaSucc = lam 'n' $ lam 'f' $ lam 'x' $
  term 'f' %% (term 'n' %% term 'f' %% term 'x')

lambdaPred :: Builder
lambdaPred = lam 'n' $ lam 'f' $ lam 'x' $
  term 'n'
  %% (lam 'g' $ lam 'h' $ term 'h' %% (term 'g' %% term 'f'))
  %% lam 'u' (term 'x')
  %% lam 'u' (term 'u')

lambdaAdd :: Builder
lambdaAdd = lam 'x' $ lam 'y' $ term 'y' %% lambdaSucc %% term 'x'

lambdaSub :: Builder
lambdaSub = lam 'x' $ lam 'y' $ term 'y' %% lambdaPred %% term 'x'

lambdaMult :: Builder
lambdaMult = lam 'x' $ lam 'y' $ lam 'f' $
  term 'x' %% (term 'y' %% term 'f')

lambdaExp :: Builder
lambdaExp = lam 'x' $ lam 'y' $ term 'y' %% term 'x'

-- ### Part 2, Excercise 3:

lambdaIsZero :: Builder
lambdaIsZero = lam 'n' $ -- n <= 0
  term 'n' %% lam 'x' lambdaFalse %% lambdaTrue

lambdaLE :: Builder
lambdaLE = lam 'm' $ lam 'n' $ -- m-n <= 0
  ap lambdaIsZero $ lambdaSub %% term 'm' %% term 'n'

lambdaEQ :: Builder
lambdaEQ = lam 'm' $ lam 'n' $
  lambdaAnd
    %% (lambdaLE %% term 'm' %% term 'n')
    %% (lambdaLE %% term 'n' %% term 'm')

lambdaGT :: Builder
lambdaGT = lam 'm' $ lam 'n' $ -- not \le
  ap lambdaNot $ lambdaLE %% term 'm' %% term 'n'

lambdaNE :: Builder
lambdaNE = lam 'm' $ lam 'n' $ -- not \eq
  ap lambdaNot $ lambdaEQ %% term 'm' %% term 'n'

lambdaGE :: Builder
lambdaGE = lam 'm' $ lam 'n' $ -- n <= m, n-m <= 0
  ap lambdaIsZero $ lambdaSub %% term 'n' %% term 'm'

lambdaLT :: Builder
lambdaLT = lam 'm' $ lam 'n' $ -- not \ge
  ap lambdaNot $ lambdaGE %% term 'm' %% term 'n'

lambdaBoolEQ :: Builder
lambdaBoolEQ = lam 'm' $ lam 'n' $ -- XNOR
  lambdaAnd
    %% (lambdaOr %% (lambdaNot %% term 'm') %% term 'n')
    %% (lambdaOr %% (lambdaNot %% term 'n') %% term 'm')

lambdaBoolNE :: Builder
lambdaBoolNE = lam 'm' $ lam 'n' $ -- XOR
  lambdaOr
    %% (lambdaAnd %% (lambdaNot %% term 'm') %% term 'n')
    %% (lambdaAnd %% (lambdaNot %% term 'n') %% term 'm')

-- ### Part 3, Excercise 1:

lambdaCons :: Builder
lambdaCons = lam 'h' $ lam 't' $ lam 'c' $ lam 'n' $
  term 'c' %% term 'h' %% (term 't' %% term 'c' %% term 'n')

lambdaNull :: Builder
lambdaNull = lam 'c' $ lam 'n' $ term 'n'

lambdaIsNull :: Builder
lambdaIsNull = lam 'l' $
  term 'l' %% lam 'h' (lam 't' lambdaFalse) %% lambdaTrue

lambdaHead :: Builder
lambdaHead = lam 'l' $
  term 'l' %% lam 'h' (lam 't' $ term 'h') %% lambdaFalse

lambdaTail :: Builder
lambdaTail = lam 'l' $ lam 'c' $ lam 'n' $
  term 'l'
  %% (lam 'h' $ lam 't' $ lam 'g' $
    term 'g' %% term 'h' %% (term 't' %% term 'c'))
  %% lam 't' (term 'n')
  %% lam 'h' (lam 't' $ term 't')

-- ### Part 3, Excercise 2:

lambdaY :: Builder
lambdaY = lam 'f' $ a %% a
  where
    a = lam 'x' $ term 'f' %% (term 'x' %% term 'x')

-- Y λgfl.IF (ISNULL l) NULL (CONS (f (HEAD l)) (g f (REST l)))
lambdaMap :: Builder
lambdaMap = ap lambdaY $ lam 'g' $ lam 'f' $ lam 'l' $
  ap (lambdaIf %% (lambdaIsNull %% term 'l') %% lambdaNull) $
    lambdaCons
    %% (term 'f' %% (lambdaHead %% term 'l'))
    %% (term 'g' %% term 'f' %% (lambdaTail %% term 'l'))

-- Y λgifl.IF (ISNULL l) i (f (g i f (REST l)) (HEAD l))
lambdaFoldr :: Builder
lambdaFoldr = ap lambdaY $ lam 'g' $ lam 'i' $ lam 'f' $ lam 'l' $
  ap (lambdaIf %% (lambdaIsNull %% term 'l') %% term 'i') $
    term 'f'
    %% (term 'g' %% term 'i' %% term 'f' %% (lambdaTail %% term 'l'))
    %% (lambdaHead %% term 'l')

-- λfl.FOLDR NULL (λae.IF (f e) (CONS e a) a) l
lambdaFilter :: Builder
lambdaFilter = lam 'f' $ lam 'l' $
    lambdaFoldr %% lambdaNull %% f %% term 'l'
  where
    f = lam 'a' $ lam 'e' $
      lambdaIf
      %% (term 'f' %% term 'e')
      %% (lambdaCons %% term 'e' %% term 'a')
      %% term 'a'
