module LambdaParser where

import Parser       hiding ((|||)) -- defined in ParserHelper
import Data.Lambda
import Data.Builder

import ParserHelper
import Lambda

-- Remember that you can (and should) define your own functions, types, and
-- parser combinators. Each of the implementations for the functions below
-- should be fairly short and concise.

{-|
    Part 1
-}

-- | Exercise 1

-- | Parses a string representing a lambda calculus expression in long form
--
-- >>> parse longLambdaP "(λx.xx)"
-- Result >< \x.xx
--
-- >>> parse longLambdaP "(λx.(λy.xy(xx)))"
-- Result >< \xy.xy(xx)
--
-- >>> parse longLambdaP "(λx(λy.x))"
-- UnexpectedChar '('
--
-- >>> parse longLambdaP " (λx. (λy. x x y))"
-- Result >< \xy.xxy
--
-- 1. brackets around all expressions:
-- >>> parse longLambdaP "(λx.λy.xxy)"
-- UnexpectedChar '('
--
-- 2. lambdas bind to only one var:
-- >>> parse longLambdaP "(λxy.xxy)"
-- UnexpectedChar '('

-- isSpace as used by space allows "\n", create our own ws parser
ws :: Parser String
ws = list1 $ oneof " \t"

alpha :: Parser Char
alpha = oneof ['a'..'z']

-- | Single-character lambda.
longLambda :: Parser Builder -> Parser Builder
longLambda p = do
  c <- betweenChar 'λ' '.' alpha -- no ws allowed
  expr <- p
  pure $ lam c expr

longItem :: Parser Builder
longItem =
  -- check for ws on rhs only when checking for further rhs chars, so as to not greedy-parse ws:
  betweenChar '(' ')' (trySecond (longLambda longExpr ||| longExpr) ws) -- corecursive: start new expr
  ||| term <$> alpha
  ||| ws *> longItem -- recursive: retry parse

longExpr :: Parser Builder
longExpr = list1 longItem >>= pure . foldl1 ap

longLambdaP :: Parser Lambda
longLambdaP = build <$> longExpr

-- | Parses a string representing a lambda calculus expression in short form
--
-- >>> parse shortLambdaP "λx.xx"
-- Result >< \x.xx
--
-- >>> parse shortLambdaP "λxy.xy(xx)"
-- Result >< \xy.xy(xx)
--
-- >>> parse shortLambdaP "λx.x(λy.yy)"
-- Result >< \x.x\y.yy
--
-- >>> parse shortLambdaP "(λx.x)(λy.yy)"
-- Result >< (\x.x)\y.yy
--
-- >>> parse shortLambdaP "λxyz"
-- UnexpectedEof

alphas :: Parser [Char]
alphas = list1 alpha

shortLambda :: Parser Builder -> Parser Builder
shortLambda p = do
  s <- betweenChar 'λ' '.' alphas -- no ws allowed
  expr <- p
  pure $ foldr lam expr s

shortItem :: Parser Builder
shortItem =
  -- check for ws on rhs only when checking for further rhs chars, so as to not greedy-parse ws - not using `betweenPaddedParens` for faithful recreation of the BNF:
  betweenChar '(' ')' (trySecond shortExpr ws)
  ||| term <$> alpha
  ||| ws *> shortItem -- recursive: retry parse
  ||| shortLambda shortExpr -- corecursive, start new expr - parse last, to pass given tests as shown

shortExpr :: Parser Builder
shortExpr = list1 shortItem >>= pure . foldl1 ap

shortLambdaP :: Parser Lambda
shortLambdaP = build <$> shortExpr

-- | Parses a string representing a lambda calculus expression in short or long form
-- >>> parse lambdaP "λx.xx"
-- Result >< \x.xx
--
-- >>> parse lambdaP "(λx.xx)"
-- Result >< \x.xx
--
-- >>> parse lambdaP "λx..x"
-- UnexpectedChar '.'
--
-- >>> parse lambdaP "λx.x "
-- Result > < \x.x
--
-- >>> parse lambdaP "λx."
-- UnexpectedEof
--
-- >>> parse longLambdaP "((λz.z)(λa.aa))(λz.z)"
-- Result >< (\z.z)(\a.aa)\z.z
-- >>> parse longLambdaP "(λa.aa)(λz.z)"
-- Result >< (\a.aa)\z.z
-- >>> parse longLambdaP "(λb.(λt.(λf.b t f)))"
-- Result >< \btf.btf
-- >>> parse longLambdaP "(λb.(λt.(λf.f t b)))"
-- Result >< \btf.ftb
-- >>> parse longLambdaP "(λf.(λx.f(xx))(λx.f(xx)))"
-- Result >< \f.(\x.f(xx))\x.f(xx)
-- >>> parse longLambdaP "(λx.(λy.xxy(λz.z)x))"
-- Result >< \xy.xxy(\z.z)x
-- >>> parse longLambdaP "(λg.(λx.g(xx))(λx.g(xx)))"
-- Result >< \g.(\x.g(xx))\x.g(xx)
-- >>> parse longLambdaP "(λx.x)"
-- Result >< \x.x
-- >>> parse longLambdaP "(λx.xx(λy.xy(λz.xzy))x)"
-- Result >< \x.xx(\y.xy\z.xzy)x
-- >>> parse longLambdaP "(λx.xx)(λx.xx)"
-- Result >< (\x.xx)\x.xx
-- >>> parse longLambdaP "(λz.z)(λa.aa)(λz.z)"
-- Result >< (\z.z)(\a.aa)\z.z
-- >>> parse longLambdaP "  (λx.(λy.xx(λz.z) x))"
-- Result >< \xy.xx(\z.z)x
-- >>> parse longLambdaP "( (λx.x(xx))(λx.x(xx)) )"
-- Result >< (\x.x(xx))\x.x(xx)
--
-- >>> parse longLambdaP "λb.λt.λf.btf"
-- UnexpectedChar '\955'
-- >>> parse longLambdaP "λbtf.b t  f"
-- UnexpectedChar '\955'
-- >>> parse longLambdaP "λmn.nm"
-- UnexpectedChar '\955'
-- >>> parse longLambdaP "λx.xx (λy.xyλyz.xzy)"
-- UnexpectedChar '\955'
-- >>> parse longLambdaP "λx.xx (λy.xyλz.xzy)"
-- UnexpectedChar '\955'
-- >>> parse longLambdaP "λx.xxx"
-- UnexpectedChar '\955'
-- >>> parse longLambdaP "λxy.(xx)y"
-- UnexpectedChar '\955'
-- >>> parse longLambdaP "λxy.x(xy)"
-- UnexpectedChar '\955'
-- >>> parse longLambdaP "(λxy.xxy)"
-- UnexpectedChar '('
-- >>> parse longLambdaP "(λxyz.xy(zx))"
-- UnexpectedChar '('
-- >>> parse longLambdaP "(λxyz.xy(zxy))"
-- UnexpectedChar '('
-- >>> parse longLambdaP "(λxyz.zx)"
-- UnexpectedChar '('
-- >>> parse longLambdaP "(λxz.xz)"
-- UnexpectedChar '('
-- >>> parse longLambdaP "(λyx.x)"
-- UnexpectedChar '('
--
-- >>> parse lambdaP "λb.λt.λf.btf"
-- Result >< \btf.btf
-- >>> parse lambdaP "λbtf.b t  f"
-- Result >< \btf.btf
-- >>> parse lambdaP "λmn.nm"
-- Result >< \mn.nm
-- >>> parse lambdaP "λx.xxx"
-- Result >< \x.xxx
-- >>> parse lambdaP "λxy.(xx)y"
-- Result >< \xy.xxy
-- >>> parse lambdaP "λxy.x(xy)"
-- Result >< \xy.x(xy)
-- >>> parse lambdaP "λxy.xxy"
-- Result >< \xy.xxy
-- >>> parse lambdaP "λxyz.zx"
-- Result >< \xyz.zx
-- >>> parse lambdaP "λxyz.xy(zx)"
-- Result >< \xyz.xy(zx)
-- >>> parse lambdaP "λxyz.xy(zxy)"
-- Result >< \xyz.xy(zxy)
-- >>> parse lambdaP "λxz.xz"
-- Result >< \xz.xz
-- >>> parse lambdaP "λyx.x"
-- Result >< \yx.x
-- >>> parse lambdaP "λz.(λx.xz)"
-- Result >< \zx.xz
-- >>> parse lambdaP "λx.xx (λy.xyλyz.xzy)"
-- Result >< \x.xx\y.xy\az.xza
-- >>> parse lambdaP "λx.xx (λy.xyλz.xzy)"
-- Result >< \x.xx\y.xy\z.xzy

lambdaP :: Parser Lambda
lambdaP = longLambdaP ||| shortLambdaP

{-|
    Part 2
-}

-- | Exercise 1

-- IMPORTANT: The church encoding for boolean constructs can be found here -> https://tgdwyer.github.io/lambdacalculus/#church-encodings

-- | Parse a logical expression and returns in lambda calculus
-- >>> lamToBool <$> parse logicP "True and False"
-- Result >< Just False
--
-- >>> lamToBool <$> parse logicP "True and False or not False and True"
-- Result >< Just True
--
-- >>> lamToBool <$> parse logicP "not not not False"
-- Result >< Just True
--
-- >>> parse logicP "True and False"
-- Result >< (\xy.(\btf.btf)xy\_f.f)(\t_.t)\_f.f
--
-- >>> parse logicP "not False"
-- Result >< (\x.(\btf.btf)x(\_f.f)\t_.t)\_f.f
--
-- >>> lamToBool <$> parse logicP "if True and not False then True or True else False"
-- Result >< Just True
--
-- >>> lamToBool <$> parse logicP "(if not( not (not ( (True and False)or False ) )) and (not (not True) or not False) then False or True else False)"
-- Result >< Just True
--
-- >>> lamToBool <$> parse logicP "not True and True or False"
-- Result >< Just False
--
-- >>> lamToBool <$> parse logicP "and"
-- UnexpectedChar 'a'
--
-- >>> lamToBool <$> parse logicP "if True then True and if True then False else True else False"
-- Result >< Just False
--
-- >>> lamToBool <$> parse logicP "if False and if True then True else False then True else if True then False else True"
-- Result >< Just False

-- | Applies two values to various lambda operators.
applyExpr :: Builder -> Parser (Builder -> Builder -> Builder)
applyExpr l = pure $ \a b -> l %% a %% b

-- tokens: case-sensitive, whitespace-delimited:
{-
if   = 'if' cond 'then' cond 'else' cond
letc = '(' cond ')' | if
bool = letc | 'True' | 'False'
not  = bool | 'not' not
and  = not  | not 'and' not
or   = and  | and 'or' and
cond = or  -- logicExpr
-}

-- restart parse co-recursively
logicEtc :: Parser Builder
logicEtc = betweenPaddedParens logicExpr ||| logicIf

logicBool :: Parser Builder
logicBool =
  strWord "True" *> pure lambdaTrue -- `*>` for low fixity
  ||| strWord "False" *> pure lambdaFalse
  ||| logicEtc

logicNot :: Parser Builder
logicNot =
  ap <$> (strWord "not" *> pure lambdaNot) <*> logicNot
  ||| logicBool

logicAnd :: Parser Builder
logicAnd = chain logicNot $ strWord "and" *> applyExpr lambdaAnd

logicOr :: Parser Builder
logicOr = chain logicAnd $ strWord "or" *> applyExpr lambdaOr

logicIf :: Parser Builder
logicIf = do
  _ <- strWord "if" -- >-Wall
  b <- logicExpr
  _ <- strWord "then" -- >-Wall
  t <- logicExpr
  _ <- strWord "else" -- >-Wall
  f <- logicExpr
  pure $ lambdaIf %% b %% t %% f

logicExpr :: Parser Builder
logicExpr = logicOr

logicP :: Parser Lambda
logicP = build <$> logicExpr

-- | Exercise 2

-- | The church encoding for arithmetic operations are given below (with x and y being church numerals)

-- | x + y = add = λxy.y succ x
-- | x - y = minus = λxy.y pred x
-- | x * y = multiply = λxyf.x(yf)
-- | x ** y = exp = λxy.yx

-- | The helper functions you'll need are:
-- | succ = λnfx.f(nfx)
-- | pred = λnfx.n(λgh.h(gf))(λu.x)(λu.u)
-- | Note since we haven't encoded negative numbers pred 0 == 0, and m - n (where n > m) = 0

-- | Parse simple arithmetic expressions involving + - and natural numbers into lambda calculus
-- >>> lamToInt <$> parse basicArithmeticP "5 + 4"
-- Result >< Just 9
--
-- >>> lamToInt <$> parse basicArithmeticP "5 + 9 - 3 + 2"
-- Result >< Just 13
--
-- >>> lamToInt <$> parse basicArithmeticP "5 - 0"
-- Result >< Just 5

barithInt :: Parser Builder
barithInt = int >>= pure . intToLam

barithExpr :: Parser Builder
barithExpr = chain barithInt $
  paddedC '+' *> applyExpr lambdaAdd
  ||| paddedC '-' *> applyExpr lambdaSub

basicArithmeticP :: Parser Lambda
basicArithmeticP = build <$> barithExpr

-- | Parse arithmetic expressions involving + - * ** () and natural numbers into lambda calculus
-- >>> lamToInt <$> parse arithmeticP "5 + 9 * 3 - 2**3"
-- Result >< Just 24
--
-- >>> lamToInt <$> parse arithmeticP "100 - 4 * 2**(4-1)"
-- Result >< Just 68

{-
aetc = '(' num ')'  -- quoted strings here assumed padded with ws
int  = aetc | [0-9]+  -- no negatives, also inaccurate (no leading 0s allowed)
exp  = int  | int '**' int
prod = exp  | exp '*' exp
sum  = prod | prod [+-] prod
num  = sum  -- arithExpr
-}

arithEtc :: Parser Builder
arithEtc = betweenPaddedParens arithExpr

arithInt :: Parser Builder
arithInt = (int >>= pure . intToLam) ||| arithEtc

arithExp :: Parser Builder
arithExp = chain arithInt $ paddedS "**" *> applyExpr lambdaExp

arithMult :: Parser Builder
arithMult = chain arithExp $ paddedC '*' *> applyExpr lambdaMult

arithSum :: Parser Builder
arithSum = chain arithMult $
  paddedC '+' *> applyExpr lambdaAdd
  ||| paddedC '-' *> applyExpr lambdaSub

arithExpr :: Parser Builder
arithExpr = arithSum

arithmeticP :: Parser Lambda
arithmeticP = build <$> arithExpr

-- | Exercise 3

-- | The church encoding for comparison operations are given below (with x and y being church numerals)
--
-- | x <= y = LEQ = λmn.isZero (minus m n)
-- | x == y = EQ = λmn.and (LEQ m n) (LEQ n m)
--
-- | The helper function you'll need is:
-- | isZero = λn.n(λx.False)True
--
-- >>> lamToBool <$> parse complexCalcP "9 - 2 <= 3 + 6"
-- Result >< Just True
--
-- >>> lamToBool <$> parse complexCalcP "15 - 2 * 2 != 2**3 + 3 or 5 * 3 + 1 < 9"
-- Result >< Just False
--
-- >>> lamToBool <$> parse complexCalcP "(1 + 3 < 5 * 3) == (7 * 2 != 4) and (3 - 1 == 5 - 3)"
-- Result >< Just True
--
-- >>> lamToBool <$> parse complexCalcP "not False == not (1 > 2) and 1 + 2 == 3"
-- Result >< Just True

-- duplicate logic parsers, change: compLogicAnd detours to compLogic:

compLogicEtc :: Parser Builder
compLogicEtc = betweenPaddedParens compExpr ||| compLogicIf

compLogicBool :: Parser Builder
compLogicBool =
  strWord "True" *> pure lambdaTrue
  ||| strWord "False" *> pure lambdaFalse
  ||| compLogicEtc

compLogicNot :: Parser Builder
compLogicNot =
  ap <$> (strWord "not" *> pure lambdaNot) <*> compLogicNot
  ||| compLogicBool

compLogicAnd :: Parser Builder
compLogicAnd = chain compLogic $ strWord "and" *> applyExpr lambdaAnd

compLogicOr :: Parser Builder
compLogicOr = chain compLogicAnd $ strWord "or" *> applyExpr lambdaOr

compLogicIf :: Parser Builder
compLogicIf = do
  _ <- strWord "if"
  b <- compExpr
  _ <- strWord "then"
  t <- compExpr
  _ <- strWord "else"
  f <- compExpr
  pure $ lambdaIf %% b %% t %% f

compLogicExpr :: Parser Builder
compLogicExpr = compLogicOr

-- new comparison parsers:
{-
s = comp
if   = 'if' comp 'then' comp 'else' comp
cetc = '(' comp ')' | if
bool = cetc | 'True' | 'False'
not  = bool | 'not' not
acmp = num  | num (#'[<>]' | #'[<>!=]' '=') num  -- new logic
comp = acmp | not                                -- new logic
lcmp = comp | comp #'[=!]' '=' comp              -- new logic
and  = lcmp | lcmp 'and' lcmp  -- lcmp/compLogic injected here
or   = and  | and 'or' and
comp = or  -- compExpr
-}

-- chain only once - >1 not possible as operator reduces int to bool, 0 not possible as complexCalc returns only bools
compArith :: Parser Builder
compArith = chain1 arithExpr $
  paddedS "==" *> applyExpr lambdaEQ
  ||| paddedS "!=" *> applyExpr lambdaNE
  ||| paddedS ">=" *> applyExpr lambdaGE
  ||| paddedS "<=" *> applyExpr lambdaLE
  ||| paddedC '>' *> applyExpr lambdaGT
  ||| paddedC '<' *> applyExpr lambdaLT

compLogic :: Parser Builder
compLogic = chain (compArith ||| compLogicNot) $
  paddedS "==" *> applyExpr lambdaBoolEQ
  ||| paddedS "!=" *> applyExpr lambdaBoolNE

-- comparison expression
compExpr :: Parser Builder
compExpr = compLogicExpr

complexCalcP :: Parser Lambda
complexCalcP = build <$> compExpr

{-|
    Part 3
-}

-- | Exercise 1

-- | The church encoding for list constructs are given below
-- | [] = null = λcn.n
-- | isNull = λl.l(λht.False) True
-- | cons = λhtcn.ch(tcn)
-- | head = λl.l(λht.h) False
-- | tail = λlcn.l(λhtg.gh(tc))(λt.n)(λht.t)
--
-- >>> parse listP "[]"
-- Result >< \cn.n
--
-- >>> parse listP "[True]"
-- Result >< (\htcn.ch(tcn))(\t_.t)\cn.n
--
-- >>> parse listP "[0, 0]"
-- Result >< (\htcn.ch(tcn))(\fx.x)((\htcn.ch(tcn))(\fx.x)\cn.n)
--
-- >>> parse listP "[0, 0"
-- UnexpectedEof

-- foldr (:) []
listToLambda :: [Builder] -> Builder
listToLambda = foldr (ap . (lambdaCons %%)) lambdaNull

{-
item = comp | num  -- valExpr
list = '[' (item | item ',' item) ']'  -- listItems -> listExpr
-}

valExpr :: Parser Builder
valExpr = compExpr ||| arithExpr

listItems :: Parser [Builder]
listItems = betweenPaddedBrackets $ sepbyC ',' valExpr

listExpr :: Parser Builder
listExpr = listItems >>= pure . listToLambda

listP :: Parser Lambda
listP = build <$> listExpr

-- |
--
-- >>> lamToBool <$> parse listOpP "head rest [True, False, True, False, False]"
-- Result >< Just False
--
-- >>> lamToBool <$> parse listOpP "isNull []"
-- Result >< Just True
--
-- >>> lamToBool <$> parse listOpP "isNull [1, 2, 3]"
-- Result >< Just False
--
-- >>> lamToInt <$> parse listOpP "head rest rest [1, 2, 3, 4, 5, 6]"
-- Result >< Just 3

{-
lfn  = 'head' | 'rest' | 'isNull'  -- listopExprItem
lop  = ('' | lfn | lfn lfn) list   -- listopExpr
-}

listopExprItem :: Parser Builder
listopExprItem =
  strWord "isNull" *> pure lambdaIsNull
  ||| strWord "head" *> pure lambdaHead
  ||| strWord "rest" *> pure lambdaTail

listopExpr :: Parser Builder
listopExpr = do
  as <- list listopExprItem
  l <- listExpr
  pure $ foldr ap l as

listOpP :: Parser Lambda
listOpP = build <$> listopExpr

-- | Exercise 2

-- | Recursive list functions (map/filter/foldl1/foldr).
-- UB if: list/lambda does not match signature as shown below:
-- (map) list: [a], lambda: (elem: a) -> a
-- (filter) list: [a], lambda: (elem: a) -> bool
-- (reduce) init: b, list: [a], lambda: (acc: b, elem: a) -> b
--
-- succ:
-- >>> lamToInt <$> parse recursiveP "head map (λnfx.f(nfx)) rest [1,2,3,4,5]"
-- Result >< Just 3
--
-- (*):
-- >>> lamToInt <$> parse recursiveP "foldr 1 (λxyf.x(yf)) [1,2,3,2,1]"
-- Result >< Just 12
--
-- not_is_zero (normalized):
-- >>> lamToInt <$> parse recursiveP "head rest filter (λn.n(λxyz.z)(λxy.x)(λxy.y)(λxy.x)) [1,0,0,4,5,6]"
-- Result >< Just 4
--
-- and (normalized):
-- >>> lamToBool <$> parse recursiveP "foldr True (λxy.xyλab.b) [True,True]"
-- Result >< Just True

{-
mfr  = 'map' | 'filter' | 'foldl1' | 'foldrone' item
lamb = '(' *any string as parsed by lambdaP* ')'
rec  = ('' | lfn | lfn lfn) mfr lamb lop  -- recurExpr
-}

recurFunc :: Parser Builder
recurFunc =
  paddedS "map" *> pure lambdaMap
  ||| paddedS "filter" *> pure lambdaFilter
  ||| (paddedS "foldr" *> valExpr >>= pure . ap lambdaFoldr)

recurExpr :: Parser Builder
recurExpr = do
  as <- list listopExprItem
  f <- recurFunc -- mfr
  l <- longExpr ||| betweenPaddedParens shortExpr -- lambda
  e <- listopExpr -- list
  pure $ foldr ap (f %% l %% e) as

recursiveP :: Parser Lambda
recursiveP = build <$> recurExpr

-- | Variables. Values are binded through the infix `=` like haskell, but like lua whitespace is not significant, so the next expression can begin right after the first one ends. Acceptable variable names are /[a-zA-Z0-9]+/.
-- UB if: fns are applied to wrong types, keywords (if then else not and or True False) or pure numerals (120, but not var120) used
--
-- -- >>> lamToBool <$> parse variableP "a=11 f=head v = rest [1,a-9,3,4]  f v"
-- Result >< Just 2

-- | O(n) lookup list (not table)
type LutVal a = (String, a)
type Lut a = [LutVal a]

-- | Return the leftmost item that matches the given key.
lutFind :: Lut a -> String -> Maybe a
lutFind ((k, v) : as) s = if k == s then Just v else lutFind as s
lutFind [] _ = Nothing

{-
key  = #'[a-zA-Z0-9]+'
-- all parsers listed in vxpr accept key where applicable:
vxpr = rec | lfn | lop | comp | num  -- varInjectedExpr
let  = key '=' vxpr  -- varAssign
var  = ('' | let | let let) vxpr
-}

-- varInjectedExpr :: Parser (Lut Builder, Builder) -> Parser (Lut Builder, Builder)
-- varInjectedExpr l =
--   varRecurExpr l
--   ||| varListopExprItem l
--   ||| varListopExpr l
--   ||| varCompExpr l
--   ||| varArithExpr l

-- varAssign :: Parser (LutVal Builder)
-- varAssign = do
--   k <- stringof alphanumeric -- not checked!
--   _ <- paddedC '='
--   v <- varInjectedExpr
--   -- return a new entry, to be appended to the left of a `list` list:
--   -- duplicates are fine, as lutFind always returns the leftmost/newest value:
--   pure (k, v)

-- varExpr :: Parser Builder
-- varExpr = list (padded varAssign) >>= varInjectedExpr >>= pure . snd

-- variableP :: Parser Lambda
-- variableP = build <$> varExpr
